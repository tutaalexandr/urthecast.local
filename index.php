<html>

<head>
  <title>Urtheflight</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/style.css">
</head>

<body>
  <header>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <img src="img/logo.png" height="80px" width="200px">
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="urtheflight.php">Sign in</a></li>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <div class="row">
    <div style="text-align:center">
      <iframe src="https://www.youtube.com/embed/euC25ZTcQmA" frameborder="0" allowfullscreen></iframe>
      <br>
      <br>
      <h2>Follow us on twitter!</h2>
    </div>
    <?php
$title = 'UrtheFlight';
$hashtags = 'Space, cast, earth, sattelite';
?>
      <div style="text-align:center">
        <a class="btn twitter_button" id="shareBut" title="Поделиться ссылкой в Твиттере" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" target="_parent"></a>
        <script>
        currentURL = document.location.href;
        var currentHREF = "http://twitter.com/share?text=<?php echo $title; ?>&via=twitterfeed&related=truemisha&hashtags=<?php echo $hashtags ?>&url=" + currentURL + "";

        var aHREF = document.getElementById("shareBut"); //or grab it by tagname etc
        aHREF.href = currentHREF;
        </script>
      </div>
  </div>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</body>

</html>
