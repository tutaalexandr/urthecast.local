  $('.expandable-control-search .expandable-control__button').click(function() {
      // $('.expandable-control-search .expandable-control__body').toggle();
      $('.expandable-control-add').removeClass('expandable-control--expanded');
      $('.expandable-control-search').toggleClass('expandable-control--expanded');
      if (!$('.expandable-control-search').hasClass('expandable-control--expanded')) {
          $("#search").val('');
          $('.expandable-control-search .geosuggest__suggests').addClass('geosuggest__suggests--hidden');
      }
  });
  $('.expandable-control-dlux .expandable-control__button').click(function() {
      // $('.expandable-control-dlux .expandable-control__body').toggle();
      $('.expandable-control-dlux').toggleClass('expandable-control-dlux--expanded');
      $('.expandable-control-dlux .expandable-control').toggleClass('expandable-control--expanded');
  });
  $('.expandable-control-music .expandable-control__button').click(function() {
      // $('.expandable-control-music .expandable-control__body').toggle();
      $('.expandable-control-music').toggleClass('expandable-control--expanded');

  });
  $('.expandable-control-add .expandable-control__button').click(function() {
      // $('.expandable-control-add .expandable-control__body').toggle();
      $('.expandable-control-search .geosuggest__suggests').addClass('geosuggest__suggests--hidden');
      $("#search").val('');
      $('.expandable-control-search').removeClass('expandable-control--expanded');
      $('.expandable-control-add').toggleClass('expandable-control--expanded');
  });
  $('.expandable-control-search .expandable-control__close').click(function(e) {
      // $('.expandable-control-search .expandable-control__body').toggle();
      $("#search").val('');

      $('.expandable-control-search .geosuggest__suggests').addClass('geosuggest__suggests--hidden');
      $('.expandable-control-search').removeClass('expandable-control--expanded');
      e.stopPropagation();
  });
  $('.expandable-control-music .expandable-control__close').click(function(e) {
      // $('.expandable-control-music .expandable-control__body').toggle();
      $('.expandable-control-music').removeClass('expandable-control--expanded');
      e.stopPropagation();
  });
  $('.expandable-control-add .expandable-control__close').click(function(e) {
      // $('.expandable-control-add .expandable-control__body').toggle();
      $('.expandable-control-add').removeClass('expandable-control--expanded');
      e.stopPropagation();
  });
  /*  $('.expandable-control-dlux').click(function() {
      $(this).addClass('expandable-control-dlux--expanded');
      $('.expandable-control-dlux .expandable-control').addClass('expandable-control--expanded')
    });*/
  $('.expandable-control-dlux .expandable-control__close').click(function(e) {
      $('.expandable-control-dlux').removeClass('expandable-control-dlux--expanded');
      $('.expandable-control-dlux .expandable-control').removeClass('expandable-control--expanded')
      e.stopPropagation();
  });

  if (localStorage.getItem('range') == undefined) {
      localStorage.setItem('range', '&cloud_coverage_gte=0&cloud_coverage_lte=20');
  }
  if (localStorage.getItem('renderer') == undefined) {
      localStorage.setItem('renderer', 'rgb');
  }
  if (localStorage.getItem('spring') == undefined) {
      localStorage.setItem('spring', '');
  }
  if (localStorage.getItem('summer') == undefined) {
      localStorage.setItem('summer', '');
  }
  if (localStorage.getItem('fall') == undefined) {
      localStorage.setItem('fall', '');
  }
  if (localStorage.getItem('winter') == undefined) {
      localStorage.setItem('winter', '');
  }
  if (localStorage.getItem('theia') == undefined) {
      localStorage.setItem('theia', '');
  }
  if (localStorage.getItem('deimos-1') == undefined) {
      localStorage.setItem('deimos-1', '');
  }
  if (localStorage.getItem('landsat-8') == undefined) {
      localStorage.setItem('landsat-8', '');
  }
  if (localStorage.getItem('lat') == undefined) {
      localStorage.setItem('lat', '47.7510741');
  }
  if (localStorage.getItem('lng') == undefined) {
      localStorage.setItem('lng', '-120.7401386');
  }
  if (localStorage.getItem('x') == undefined) {
      localStorage.setItem('x', 0 );
  }
  if (localStorage.getItem('y') == undefined) {
      localStorage.setItem('y', 0 );
  }
  if (localStorage.getItem('delay') == undefined) {
      localStorage.setItem('delay', 30 );
  }
  if (localStorage.getItem('uc_api_key') == undefined) {
      localStorage.setItem('uc_api_key', '27F68EF1D6804C66B859');
  }
  if (localStorage.getItem('uc_api_secret') == undefined) {
      localStorage.setItem('uc_api_secret', '9103859702034C46B44399D7F2CE4ADC')
  }
  if (localStorage.getItem('lat_search') == undefined) {
      localStorage.setItem('lat_search', '')
  }
  if (localStorage.getItem('lng_search') == undefined) {
      localStorage.setItem('lng_search', '')
  }
  $("#enter").click(function() {
      var uc_api_key = $("#uc_api_key").val();
      var uc_api_secret = $("#uc_api_secret").val();
      localStorage.setItem('uc_api_key', uc_api_key);
      localStorage.setItem('uc_api_secret', uc_api_secret)
      window.location.reload();
  })
  $("#uc_api_key").val(localStorage.getItem('uc_api_key'));
  $("#uc_api_secret").val(localStorage.getItem('uc_api_secret'));

  $("#clear").click(function() {
      localStorage.setItem('range', '&cloud_coverage_gte=0&cloud_coverage_lte=20');
      localStorage.setItem('renderer', 'rgb');
      localStorage.setItem('spring', '');
      localStorage.setItem('summer', '');
      localStorage.setItem('fall', '');
      localStorage.setItem('winter', '');
      localStorage.setItem('theia', '');
      localStorage.setItem('deimos-1', '');
      localStorage.setItem('landsat-8', '');
      localStorage.setItem('x', 0);
      localStorage.setItem('y', 0);
      localStorage.setItem('delay', 30 );
      window.location.reload();

  })

  $('.slider__range').change(function() {
      var thisRange = $(this);
      localStorage.setItem('range', '&cloud_coverage_gte=0&cloud_coverage_lte=' + thisRange.val() + '');
      console.log(localStorage.getItem('range'));
      $('.slider__output.u-right span').first().text(thisRange.val());
  });

  $('.speed-range').change(function() {
      var thisRange = $(this).val();
      clearInterval(animInterval);
      localStorage.setItem('delay', 65 - thisRange);
      animInterval = setInterval(animMap, localStorage.getItem('delay') );
     // console.log(localStorage.getItem('delay'));
      $('.speed-value').text(thisRange);
  });


  var clickPause = true;
  var cacheX;
  var cacheY;
  $('input[name="direction"]').click(function() {
      switch ($(this).attr('id')) {
          case 'top-left':
              clickPause = true;
              cacheX = localStorage.getItem('x');
              cacheY = localStorage.getItem('y');
              localStorage.setItem('x', -1);
              localStorage.setItem('y', -1);
              break;
          case 'top':
              clickPause = true;
              cacheX = localStorage.getItem('x');
              cacheY = localStorage.getItem('y');
              localStorage.setItem('x', 0);
              localStorage.setItem('y', -1);
              break;
          case 'top-right':
              clickPause = true;
              cacheX = localStorage.getItem('x');
              cacheY = localStorage.getItem('y');
              localStorage.setItem('x', 1);
              localStorage.setItem('y', -1);
              break;
          case 'left':
              clickPause = true;
              cacheX = localStorage.getItem('x');
              cacheY = localStorage.getItem('y');
              localStorage.setItem('x', -1);
              localStorage.setItem('y', 0);
              break;
          case 'right':
              clickPause = true;
              cacheX = localStorage.getItem('x');
              cacheY = localStorage.getItem('y');
              localStorage.setItem('x', 1);
              localStorage.setItem('y', 0);
              break;
          case 'bottom-left':
              clickPause = true;
              cacheX = localStorage.getItem('x');
              cacheY = localStorage.getItem('y');
              localStorage.setItem('x', -1);
              localStorage.setItem('y', 1);
              break;
          case 'bottom':
              clickPause = true;
              cacheX = localStorage.getItem('x');
              cacheY = localStorage.getItem('y');
              localStorage.setItem('x', 0);
              localStorage.setItem('y', 1);
              break;
          case 'bottom-right':
              clickPause = true;
              cacheX = localStorage.getItem('x');
              cacheY = localStorage.getItem('y');
              localStorage.setItem('x', 1);
              localStorage.setItem('y', 1);
              break;
          case 'pause':
              if (clickPause) {
                  cacheX = localStorage.getItem('x');
                  cacheY = localStorage.getItem('y');
                  localStorage.setItem('x', 0);
                  localStorage.setItem('y', 0);
                  clickPause = false;
                  break;
              } else {
                  localStorage.setItem('x', cacheX);
                  localStorage.setItem('y', cacheY);
                  clickPause = true;
                  break;
              }
      }
  });
  $('input[type=checkbox], input[type=radio]').change(function() {
      var test = $(this).attr('name');
      var url = "";
      switch (test) {
          case 'spring':
              if (this.checked) {
                  localStorage.setItem('spring', '&season=spring');
              } else {
                  localStorage.setItem('spring', '');
              }
              break;
          case 'summer':
              if (this.checked) {
                  localStorage.setItem('summer', '&season=summer');
              } else {
                  localStorage.setItem('summer', '');
              }
              break;
          case 'fall':
              if (this.checked) {
                  localStorage.setItem('fall', '&season=fall');
              } else {
                  localStorage.setItem('fall', '');
              }
              break;
          case 'winter':
              if (this.checked) {
                  localStorage.setItem('winter', '&season=winter');
              } else {
                  localStorage.setItem('winter', '');
              }
              break;
          case 'theia':
              if (this.checked) {
                  localStorage.setItem('theia', '&sensor_platform=theia');
              } else {
                  localStorage.setItem('theia', '');
              }
              break;
          case 'deimos-1':
              if (this.checked) {
                  localStorage.setItem('deimos-1', '&sensor_platform=deimos-1');
              } else {
                  localStorage.setItem('deimos-1', '');
              }
              break;
          case 'landsat-8':
              if (this.checked) {
                  localStorage.setItem('landsat-8', '&sensor_platform=landsat-8');
              } else {
                  localStorage.setItem('landsat-8', '');
              }
              break;
          case 'renderer':
              localStorage.setItem('renderer', $(this).val());
              console.log(localStorage.getItem('renderer'));

              break;
      }
  });
  // console.log(localStorage.getItem('spring')+localStorage.getItem('summer'));

  function call(id) {
      localStorage.setItem('lat', $("#lat" + id + "").text());
      localStorage.setItem('lng', $("#lng" + id + "").text());
      var url = "index.php";
      window.location.reload();
  };
  $("#search").keydown(function() {
      if ($("#search").val() == '') {
          $(".geosuggest__suggests").addClass("geosuggest__suggests--hidden");
      }
      var search = $("#search").val();
      var url = $("#search").val();
      $.ajax({
          type: "POST",
          dataType: "json",
          url: "include/geocoding.php",
          data: {
              "search": search,
              "url": url,
          },
          success: function(date) {
              $(".geosuggest__suggests").removeClass("geosuggest__suggests--hidden");
              $(".search").html(date);
          },
          error: function(e) {}
      });
  })

  $("#search_lng_lat").click(function() {
      var lat_input = $("#lat_input").val();
      var lng_input = $("#lng_input").val();
      $.ajax({
          type: "POST",
          dataType: "json",
          url: "include/geocoding.php",
          data: {
              "lat_input": lat_input,
              "lng_input": lng_input,
          },
          success: function(date) {
              $(".lat_lng").removeClass("geosuggest__suggests--hidden");
              $(".lat_lng").html(date);
          },
          error: function(e) {}
      });
  })
function lng_lat_search(lng , lat){
    var lat_input = localStorage.getItem('lat_search');
    var lng_input = localStorage.getItem('lng_search');
   // $('.coordinates').html(lat_input+', ' + lng_input);
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "include/geocoding.php",
        data: {
            "lat_input": lat_input,
            "lng_input": lng_input,
            "lng_lat_search": true,
        },
        success: function(date) {
            $(".coordinates").html(date);
        },
        error: function(e) {}
    });
}

  // Confirm we've got 'em by displaying them to the screen
  var apiKey = localStorage.getItem('uc_api_key'),
      apiSecret = localStorage.getItem('uc_api_secret'),
      url = localStorage.getItem('spring') + localStorage.getItem('summer') + localStorage.getItem('fall') + localStorage.getItem('winter') + localStorage.getItem('theia') + localStorage.getItem('deimos-1') + localStorage.getItem('landsat-8') + localStorage.getItem('range'),
      renderer = localStorage.getItem('renderer');
  // Create a Leaflet map
  var map = L.map('map').setView([
      localStorage.getItem('lat'),
      localStorage.getItem('lng')
  ], 10);
  // Create a simple UC tile layer - global map, no restrictions
  var url = `https://tile-{s}.urthecast.com/v1/${renderer}/{z}/{x}/{y}?api_key=${apiKey}&api_secret=${apiSecret}${url}`;
  // Append it to the map
  var ucTiles = L.tileLayer(url).addTo(map);

  var someValueX = 0;

  function getSupportedPropertyName(properties) {
      for (var i = 0; i < properties.length; i++) {
          if (typeof document.body.style[properties[i]] != "undefined") {
              return properties[i];
          }
      }
      return null;
  }
  var transform = ["transform", "msTransform", "webkitTransform", "mozTransform", "oTransform"];
  var item = document.querySelector(".leaflet-map-pane");
  var transformProperty = getSupportedPropertyName(transform);

  function animMap() {
      var x = localStorage.getItem('x');
      var y = localStorage.getItem('y');
      map.panBy([x, y], {
          "animate": false,
          "duration": 1.2,
          "easeLinearity": 1
      });
      localStorage.setItem('lat_search', map.getCenter().lat);
      localStorage.setItem('lng_search', map.getCenter().lng);
  };
  $('.speed-range').val(30);
  $('.speed-value').text('30');
  var animInterval = setInterval(animMap, localStorage.getItem('delay'));
 // $('.coordinates').html(map.getCenter().lat+', ' + map.getCenter().lng);
  lng_lat_search();
  setInterval(lng_lat_search , 10000);