<html>

<head>
  <title>urtheflight</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="include/leaflet.css">
  <link rel="stylesheet" href="include/developer-center.css">
  <link rel="stylesheet" href="include/catalog.css">
  <script type="text/javascript" src="include/leaflet.js"></script>
  <script language="javascript" type="text/javascript" src="include/swfobject.js"></script>
  <script src="js/admin.js"></script>
</head>

<body>
  <a href="#" class="logo"><img src="include/logo.png" alt="Urtheflight" height="100"></a>
  <div class="map-controls map-controls--expanded">
    <div class="expandable-control-dlux">
      <div class="expandable-control expandable-control--light-grey">
        <div class="u-media">
          <div class="u-media__icon">
            <button class="expandable-control__button icon-container--hover"><i class="icon icon-filters-grey"></i><i class="icon icon-filters-grey"></i></button>
          </div>
          <div class="u-media__body expandable-control__body">
            <div class="text-center">Filters</div>
            <button class="expandable-control__close">×</button>
          </div>
        </div>
      </div>
      <div class="expandable-control-dlux__body">
        <div class="filters" style="max-height: 496px;">
          <form>
            <div class="u-padding--10 u-padding-right--30 u-bordered-list-item--bottom">
              <div class="custom-controls">
                <div class="u-media__body u-cursor-pointer">
                  <div class="u-truncate u-vertical-align--middle" style="width:150px;">
                    <span>Arrows</span><span></span>
                  </div>
                  <ul class="filter-direction">
                    <li>
                      <input type="radio" name="direction" id="top-left">
                      <label for="top-left"><img src="include/arrow.png" width="25" alt="top-left"></label>
                    </li>
                    <li>
                      <input type="radio" name="direction" id="top">
                      <label for="top"><img src="include/arrow.png" width="25" alt="top"></label>
                    </li>
                    <li>
                      <input type="radio" name="direction" id="top-right">
                      <label for="top-right"><img src="include/arrow.png" width="25" alt="top-right"></label>
                    </li>
                    <br>
                    <li>
                      <input type="radio" name="direction" id="left">
                      <label for="left"><img src="include/arrow.png" width="25" alt="left"></label>
                    </li>
                    <li>
                      <input type="radio" name="direction" id="pause">
                      <label for="pause" class="pause-button"></label>
                    </li>
                    <li>
                      <input type="radio" name="direction" id="right">
                      <label for="right"><img src="include/arrow.png" width="25" alt="right"></label>
                    </li>
                    <br>
                    <li>
                      <input type="radio" name="direction" id="bottom-left">
                      <label for="bottom-left"><img src="include/arrow.png" width="25" alt="bottom-left"></label>
                    </li>
                    <li>
                      <input type="radio" name="direction" id="bottom">
                      <label for="bottom"><img src="include/arrow.png" width="25" alt="bottom"></label>
                    </li>
                    <li>
                      <input type="radio" name="direction" id="bottom-right">
                      <label for="bottom-right"><img src="include/arrow.png" width="25" alt="bottom-right"></label>
                    </li>
                  </ul>
                </div>
                <div class="u-media__body u-cursor-pointer">
                  <div class="u-truncate u-vertical-align--middle" style="width:150px;">
                    <span>Speed</span>
                  </div>
                  <input type="range" name="speed-range" class="speed-range" min="15" max="50">
                  <span class="speed-value"></span>
                </div>
              </div>
              <div class="u-media__body u-cursor-pointer">
                <div>
                  <span>Api key & Api secret</span>
                </div>
                api key<input id="uc_api_key" type="text" name="uc_api_key"> api secret <input id="uc_api_secret" type="text" name="uc_api_secret">
              </div>
              <div class="u-padding--10 u-padding-right--30 u-bordered-list-item--bottom">
                <div>
                  <div class="u-media__icon u-padding-right--30">
                    <div class="text-center" style="width:100px;"><i class="icon-season" style="display:block;margin:auto;"></i><a href="" target="_blank"><code class="inline-code--blue">season</code></a></div>
                  </div>
                  <div class="u-media__body u-cursor-pointer">
                    <div class="u-truncate u-vertical-align--middle" style="width:150px;">
                      <span>Seasons</span><span></span>
                    </div>
                    <!-- <i class="icon icon-dropdown-arrow pull-right is-open"></i> -->
                  </div>
                </div>
                <div class="u-padding-top--20 u-padding-left--130" style="display: block;">
                  <div style="margin-bottom:20px;">
                    <input id="spring" name="spring" type="checkbox" value="&season=spring">
                    <label for="spring"><span>Spring</span><span> </span><span class="u-grey--light"></span></label>
                  </div>
                  <div style="margin-bottom:20px;">
                    <input id="summer" name="summer" type="checkbox" value="&season=summer">
                    <label for="summer"><span>Summer</span><span> </span><span class="u-grey--light"></span></label>
                  </div>
                  <div style="margin-bottom:20px;">
                    <input id="fall" name="fall" type="checkbox" value="&season=fall">
                    <label for="fall"><span>Fall</span><span> </span><span class="u-grey--light"></span></label>
                  </div>
                  <div style="margin-bottom:20px;">
                    <input id="winter" name="winter" type="checkbox" value="&season=winter">
                    <label for="winter"><span>Winter</span><span> </span><span class="u-grey--light"></span></label>
                  </div>
                </div>
              </div>
              <div class="u-padding--10 u-padding-right--30 u-bordered-list-item--bottom">
                <div class="u-media__icon u-padding-right--30">
                  <div class="text-center" style="width:100px;"><i class="icon-cloud-coverage" style="display:block;margin:auto;"></i><a href="" target="_blank"><code class="inline-code--blue" >cloud_coverage</code></a></div>
                </div>
                <div class="u-media__body">
                  <div class="u-position--relative">
                    <input class="slider__range" name="range" type="range" value="20" min="0" max="100" step="1" data="&cloud_coverage_gte=0&cloud_coverage_lte=">
                    <div class="slider__output u-position--absolute u-left">
                      <span>0</span>
                      <span class="u-padding-left--5 u-grey--light">%</span></div>
                    <div class="slider__output u-position--absolute u-right"><span>20</span><span class="u-padding-left--5 u-grey--light">%</span></div>
                  </div>
                </div>
              </div>
              <div class="u-padding--10 u-padding-right--30 u-bordered-list-item--bottom">
                <div>
                  <div class="u-media__icon u-padding-right--30">
                    <div class="text-center" style="width:100px;"><i class="icon-sensor-platform" style="display:block;margin:auto;"></i><a href="" target="_blank"><code class="inline-code--blue" >sensor_platform</code></a></div>
                  </div>
                  <div class="u-media__body u-cursor-pointer">
                    <div class="u-truncate u-vertical-align--middle" style="width:150px;"><span>All Sensor Platforms</span><span> </span></div>
                  </div>
                </div>
                <div class="u-padding-top--20 u-padding-left--130" style="display: block;">
                  <div style="margin-bottom:20px;">
                    <input id="all" name="all" type="checkbox" checked="" value="">
                    <label for="all"><span>ALL</span><span> </span><span class="u-grey--light"></span></label>
                  </div>
                  <div style="margin-bottom:20px;">
                    <input id="theia" name="theia" type="checkbox" value="&sensor_platform=theia">
                    <label for="theia"><span>Theia</span><span> </span><span class="u-grey--light">5 meter</span></label>
                  </div>
                  <div style="margin-bottom:20px;">
                    <input id="deimos-1" name="deimos-1" type="checkbox" value="&sensor_platform=deimos-1">
                    <label for="deimos-1"><span>Deimos-1</span><span> </span><span class="u-grey--light">22 meter</span></label>
                  </div>
                  <div style="margin-bottom:20px;">
                    <input id="landsat-8" name="landsat-8" type="checkbox" value="&sensor_platform=landsat-8">
                    <label for="landsat-8"><span>Landsat-8</span><span> </span><span class="u-grey--light">15 meter (pansharpened)</span></label>
                  </div>
                </div>
              </div>
              <div class="u-padding--10 u-padding-right--30 u-bordered-list-item--bottom">
                <div>
                  <div class="u-media__icon u-padding-right--30">
                    <div class="text-center" style="width:100px;"><i class="icon-renderer" style="display:block;margin:auto;"></i><a href="https://developers.urthecast.com/docs/map-tiles#renderer" target="_blank"><code class="inline-code--blue">{RENDERER}</code></a></div>
                  </div>
                  <div class="u-media__body u-cursor-pointer">
                    <div class="u-truncate u-vertical-align--middle" style="width:150px;"><span>RGB</span><span> </span></div>
                  </div>
                </div>
                <div class="u-padding-top--20 u-padding-left--130" style="display: block;">
                  <div class="u-margin-bottom--20">
                    <input id="rgb" name="renderer" type="radio" checked="" value="rgb" checked>
                    <label for="rgb"><span>RGB</span><span> </span><span class="u-grey--light">Red, green, blue spectral bands</span></label>
                  </div>
                  <div class="u-margin-bottom--20">
                    <input id="ndvi" name="renderer" type="radio" value="ndvi">
                    <label for="ndvi"><span>NDVI</span><span> </span><span class="u-grey--light">Normalized Difference Vegetation Index</span></label>
                  </div>
                  <div class="u-margin-bottom--20">
                    <input id="evi" name="renderer" type="radio" value="evi">
                    <label for="evi"><span>EVI</span><span> </span><span class="u-grey--light">Enhanced Vegetation Index</span></label>
                  </div>
                  <div class="u-margin-bottom--20">
                    <input id="ndwi" name="renderer" type="radio" value="ndwi">
                    <label for="ndwi"><span>NDWI</span><span> </span><span class="u-grey--light">Normalized Difference Water Index</span></label>
                  </div>
                  <div class="u-margin-bottom--20">
                    <input id="false-color-nir" name="renderer" type="radio" value="false-color-nir">
                    <label for="false-color-nir"><span>False-Color NIR</span><span> </span><span class="u-grey--light">Near-infrared, red, green spectral bands</span></label>
                  </div>
                </div>
              </div>
          </form>
          <div id="enter">ENTER</div>
          <div id="clear">CLEAR</div>
          </div>
        </div>
      </div>
    </div>
    <div class="expandable-control expandable-control-music">
      <div class="u-media">
        <div class="u-media__icon">
          <button class="expandable-control__button icon-container--hover"><i class="icon icon--music"></i><i class="icon icon--music"></i></button>
        </div>
        <div class="u-media__body expandable-control__body">
          <div class="geosuggest ">
            <div id="player">
              <h1>No flash player!</h1>
              <p>It looks like you don't have flash player installed. <a href="http://www.macromedia.com/go/getflashplayer">Click here</a> to go to Macromedia download page.</p>
            </div>
          </div>
          <!-- <button class="expandable-control__close">×</button> -->
        </div>
      </div>
    </div>
    <div class="expandable-control expandable-control-search">
      <div class="u-media">
        <div class="u-media__icon">
          <button class="expandable-control__button icon-container--hover"><i class="icon icon-search-white"></i><i class="icon icon-search-grey"></i></button>
        </div>
        <div class="u-media__body expandable-control__body">
          <div class="geosuggest ">
            <input class="geosuggest__input" type="text" name="search" id="search" value='' placeholder="Search places">
            <ul class="geosuggest__suggests search geosuggest__suggests--hidden"></ul>
          </div>
          <button class="expandable-control__close">×</button>
        </div>
      </div>
    </div>
    <div class="expandable-control expandable-control-add">
      <div class="u-media">
        <div class="u-media__icon">
          <button class="expandable-control__button icon-container--hover"><i class="icon icon-search-white"></i><i class="icon icon-search-grey"></i></button>
        </div>
        <div class="u-media__body expandable-control__body">
          <div class="geosuggest">
            <ul>
              <li>
                <input class="geosuggest__input" type="text" name="lat" id="lat_input" value='' placeholder="lat">
              </li>
              <li>
                <input class="geosuggest__input" type="text" name="lng" id="lng_input" value='' placeholder="lng">
              </li>
              <li>
                <button id='search_lng_lat' style="width: 100%">SEARCH</button>
              </li>
            </ul>
            <ul class="geosuggest__suggests lat_lng geosuggest__suggests--hidden"></ul>
          </div>
          <button class="expandable-control__close">×</button>
        </div>
      </div>
    </div>
  </div>
  <div class="coordinates"></div>
  <div id="map"></div>
  <script language="javascript" type="text/javascript" src="include/jqueryrush.js"></script>
  <script language="javascript" type="text/javascript" src="player/player.js"></script>
</body>

</html>
